<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";

    protected $guarded = [];

    /*
     * eloquent relations
     */

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function reserves()
    {
        return $this->hasMany(Reserve::class);
    }
}
