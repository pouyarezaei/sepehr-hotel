<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $table = "managers";

    /*
     * eloquent relations
     */

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
