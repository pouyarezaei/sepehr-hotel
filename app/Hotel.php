<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = "hotels";

    /*
     * eloquent relations
     */

    public function manager()
    {
        return $this->hasOne(Manager::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function staffs()
    {
        return $this->hasMany(Staff::class);
    }

    public function reserves()
    {
        return $this->hasMany(Reserve::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
