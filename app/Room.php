<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = "rooms";

    /*
     * eloquent relations
     */

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
