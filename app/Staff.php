<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = "staff";

    /*
     * eloquent relations
     */

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
