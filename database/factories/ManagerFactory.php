<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Manager;
use Faker\Generator as Faker;

$factory->define(Manager::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'hotel_id' => factory(\App\Hotel::class),
        'password' => 'password'
    ];
});
