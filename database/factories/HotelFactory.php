<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hotel;
use App\Manager;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'star' => $faker->randomNumber(1),
        'number_of_rooms' => $faker->randomNumber(1),
    ];
});
