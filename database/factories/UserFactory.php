<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'password' => 'password',
        'code' => $faker->unique()->postcode,
        'phone' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->email,
        'avatar' => 'https://api.adorable.io/avatars/' . $faker->randomNumber(4),
    ];
});
