<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Reserve;
use App\Room;
use Faker\Generator as Faker;

$factory->define(Reserve::class, function (Faker $faker) {
    return [
        'room_id' => factory(Room::class),
        'hotel_id' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1, 10),
        'reservation_at' => $faker->dateTime,
        'reservation_to' => $faker->dateTime,
    ];
});
