<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hotel;
use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        'hotel_id' => factory(Hotel::class),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'job' => $faker->randomElement(['chef', 'supervisor', 'maid', 'waiter']),
        'password' => 'password'
    ];
});
