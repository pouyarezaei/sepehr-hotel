<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'number' => $faker->randomNumber(1),
        'capacity' => $faker->randomNumber(1),
        'floor' => $faker->randomNumber(1),
        'cost_per_day' => $faker->randomNumber(6),
    ];
});
