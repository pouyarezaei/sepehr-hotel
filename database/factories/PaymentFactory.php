<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Payment;
use App\Room;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'room_id' => factory(Room::class),
        'user_id' => $faker->numberBetween(1, 10),
        'hotel_id' => $faker->numberBetween(1, 10),
        'paid' => $faker->randomNumber(6),
    ];
});
