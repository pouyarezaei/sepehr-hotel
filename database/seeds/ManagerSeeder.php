<?php

use App\Hotel;
use App\Manager;
use Illuminate\Database\Seeder;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hotel::all()->each(function ($hotel) {
            factory(Manager::class)->create(['hotel_id' => $hotel->id]);
        });
    }
}
