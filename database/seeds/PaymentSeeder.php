<?php

use App\Hotel;
use App\Payment;
use App\Room;
use App\User;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Payment::class, 50)->create([
            'room_id' => Room::inRandomOrder()->first()->id,
        ]);
    }

}
