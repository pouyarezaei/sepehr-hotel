<?php

use App\Hotel;
use App\Staff;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hotel::all()->each(function ($hotel) {
            factory(Staff::class, 3)->create(['hotel_id' => $hotel->id]);
        });
    }
}
