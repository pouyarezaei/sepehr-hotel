<?php

use App\Reserve;
use App\Room;
use Illuminate\Database\Seeder;

class ReserveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Reserve::class, 50)->create([
            'room_id' => Room::all()->random(1)->id
        ]);

    }
}
