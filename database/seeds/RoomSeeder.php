<?php

use App\Hotel;
use App\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hotel::all()->each(function ($hotel) {
            factory(Room::class, 5)->create(['hotel_id' => $hotel->id]);
        });
    }
}
