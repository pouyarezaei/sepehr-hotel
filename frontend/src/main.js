import Vue from 'vue'
import App from './App.vue'
import router from './router'
import moment from 'moment'
import {createProvider} from './vue-apollo'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faStar} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import VueGeolocation from 'vue-browser-geolocation';
import * as VueGoogleMaps from 'vue2-google-maps'

library.add(faStar)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueGeolocation);

Vue.prototype.moment = moment
Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyB7M9ryyPgIjKvR4GorbDSp7MMH-wJTzx8',
    },
})
new Vue({
    router,
    apolloProvider: createProvider(),
    render: h => h(App)
}).$mount('#app')
